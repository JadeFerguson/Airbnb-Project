This is project [Next.js](https://nextjs.org/) was bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Overview
This project is a front end clone of Airbnb to learn React.js, Next.js, and Tailwindcss.

## Resources
- [Tailwind Install](https://tailwindcss.com/docs/guides/nextjs)
- [Hero Icons](https://heroicons.com/) for search bar icon
- [API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`
- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
- [Date Range React Picker](https://github.com/hypeserver/react-date-range)
